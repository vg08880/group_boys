@font-face {
    font-family: 'Bazar';
    src: url('Bazar.eot');
    src: url('Bazar.woff2') format('woff2'),
         url('Bazar.woff') format('woff'),
         url('Bazar.ttf') format('truetype'),
         url('Bazar.svg#Bazar') format('svg'),
         url('Bazar.eot?#iefix') format('embedded-opentype');
    font-weight: normal;
    font-style: normal;
  }
  
  @font-face {
    font-family: 'Oswald-Bold';
    src: url('Oswald-Bold.eot');
    src: url('Oswald-Bold.woff2') format('woff2'),
         url('Oswald-Bold.woff') format('woff'),
         url('Oswald-Bold.ttf') format('truetype'),
         url('Oswald-Bold.svg#Oswald-Bold') format('svg'),
         url('Oswald-Bold.eot?#iefix') format('embedded-opentype');
    font-weight: normal;
    font-style: normal;
  }
  
  @font-face {
    font-family: 'Oswald-Regular';
    src: url('Oswald-Regular.eot');
    src: url('Oswald-Regular.woff2') format('woff2'),
         url('Oswald-Regular.woff') format('woff'),
         url('Oswald-Regular.ttf') format('truetype'),
         url('Oswald-Regular.svg#Oswald-Regular') format('svg'),
         url('Oswald-Regular.eot?#iefix') format('embedded-opentype');
    font-weight: normal;
    font-style: normal;
  }
  
  @font-face {
    font-family: 'RobotoSlab-Bold';
    src: url('RobotoSlab-Bold.eot');
    src: url('RobotoSlab-Bold.woff2') format('woff2'),
         url('RobotoSlab-Bold.woff') format('woff'),
         url('RobotoSlab-Bold.ttf') format('truetype'),
         url('RobotoSlab-Bold.svg#RobotoSlab-Bold') format('svg'),
         url('RobotoSlab-Bold.eot?#iefix') format('embedded-opentype');
    font-weight: normal;
    font-style: normal;
  }
  
  @font-face {
    font-family: 'RobotoSlab-Regular';
    src: url('RobotoSlab-Regular.eot');
    src: url('RobotoSlab-Regular.woff2') format('woff2'),
         url('RobotoSlab-Regular.woff') format('woff'),
         url('RobotoSlab-Regular.ttf') format('truetype'),
         url('RobotoSlab-Regular.svg#RobotoSlab-Regular') format('svg'),
         url('RobotoSlab-Regular.eot?#iefix') format('embedded-opentype');
    font-weight: normal;
    font-style: normal;
  }
  
  @font-face {
    font-family: 'SeasideResortNF';
    src: url('SeasideResortNF.eot');
    src: url('SeasideResortNF.woff2') format('woff2'),
         url('SeasideResortNF.woff') format('woff'),
         url('SeasideResortNF.ttf') format('truetype'),
         url('SeasideResortNF.svg#SeasideResortNF') format('svg'),
         url('SeasideResortNF.eot?#iefix') format('embedded-opentype');
    font-weight: normal;
    font-style: normal;
  }
  
